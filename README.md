# Cotizador-cos

App web de cotizador

## Instalacion e integración

El pruyecto tiene una integración con composer y phpunit. Para Instalar el proyecto debe ejecutar 

`composer install `

## Test de pruebas unitarias

La integracion de php unit permite hacer pruebas unitarias y de integracion para ejecutar los test del proyecto ejecute el siguiente comando.

`./vendor/bin/phpunit tests`

Se mostrara por consola el siguiente aviso al ejecutar.

![](/testing.PNG)

Las clases que se ejecutan son las siguientes.

![](/Class.PNG)

Un ejemplo de como se aplica php unit es tomando como referencia la clase RenderViewTest.php la cual valida si la vista existe para su renderizacion en pantalla.

![](/ClassTesting.PNG)


CI/CD proyect Cotizador by Raul Pech




